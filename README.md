# Rights In Records By Design

This is the publication repository for the _Rights in Records by Design_ ARC project.

Each sub-directory contains a metadata statement and a publicly shareable version of the publication --- either an open access final version, or a pre-publication review version.

For more information, please contact 

[Joanne Evans](mailto:joanne.evans@monash.edu)

[Gregory Rolan](mailto:greg.rolan@monash.edu)

![logo](./RiRbD-Logo.png)

https://rights-records.it.monash.edu/research-development-agenda/rights-in-records-by-design/
